<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('permission:create-post');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/permission','PermissionController@index')->name('permission');
Route::get('/role','RoleController@index')->name('role');
Route::get('/role/attach','RoleController@attach')->name('attachrole');

// Route::get('/product/add','ProductController@getAdd')->name('addProduct');
Route::resource('/product','ProductController');
