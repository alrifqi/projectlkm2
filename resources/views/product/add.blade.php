@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Product</div>
                <div class="panel-body">
                  <form method="post" action="{{ url('product') }}" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="form-group">
                      <label for="exampleInputEmail1">Product Name</label>
                      <input type="product name" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    </div>
                    
                    <div class="row" id="row-attach">
                      <div class="col-md-4 col-attach" style="margin: 10px 0px;">
                        <input type="file" class="dropify" data-min-width="400" name="attachment[]"/>  
                      </div>
                    </div>
                    <button type="button" id="btn-add" class="btn btn-primary">Add</button>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extra_js')
<script>
    $('.dropify').dropify();

    $('#btn-add').on('click', function(){
      // var clone = $( ".col-attach:first" ).clone().appendTo( "#row-attach" );
      // clone.find('input').removeClass('dropify');
      // clone.find('input').addClass('dropify');
      console.log('aa');
      $('#row-attach').append('<div class="col-md-4 col-attach" style="margin: 10px 0px;"><input type="file" class="dropify" data-min-width="400" name="attachment[]"/></div>')
      $('.dropify').dropify();
    });

</script>
@endsection