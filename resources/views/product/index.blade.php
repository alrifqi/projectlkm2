@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(CheckPermission::checkInView('create-post'))
              <a href="{{ route('product.create') }}" class="btn btn-default pull-right" style="margin: 5px 5px;">Add</a>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Product</div>
                <div class="panel-body">
                    <table class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <td>No</td>
                          <td>Product</td>
                          <td>Action</td>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($attachments as $a)
                        <tr>
                          <td>{{ $a['id'] }}</td>
                          <td>{{ $a['file_name'] }}</td>
                          <td>
                          @if(CheckPermission::checkInView('delete-post'))
                            <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                          @endif
                          @if(CheckPermission::checkInView('update-post'))
                            <a class="btn btn-default btn-xs" aria-label="Left Align" href="{{ route('product.edit', array('product'=>$a['id'])) }}">
                              <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </a>
                          @endif
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection