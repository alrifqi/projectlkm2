<?php
namespace App\Helpers;

use Auth;
use App;
class CheckPermission{
  public function __construct(){

  }

  static function check($permission){
    if(!Auth::user()->can($permission)){
      App::abort(403);
    }
    return Auth::user()->can('create-post');
  }

  static function checkInView($permission){
    if(!Auth::user()->can($permission)){
      return false;
    }
    return true;
  }
}