<?php
namespace App\Helpers;

use Auth;
use App;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;

class FileUploader{
  public function __construct(){
    $this->manager = new ImageManager(array('driver' => 'imagick'));
    $this->image_type = array('jpg','png','bmp','jpeg');
  }
  public function upload($payload){
    $folder = $this->checkFolder();
    $filename = $this->generateFilename($payload);
    $data = array();
    $data['filetype'] = $payload->getClientOriginalExtension();
    $data['original_name'] = $payload->getClientOriginalName();
    $data['edited_filename'] = $filename;
    $data['path'] = $folder.'/'.$filename;
    if(in_array($data['filetype'], $this->image_type)) {
      $imgsize = getimagesize($payload);
      $data['original_width'] = $imgsize[0];
      $data['original_height'] = $imgsize[1];
      $this->moveFile($payload, $folder, $filename);
      $editedFile = $this->resizeImage($data, $payload, $folder); 
    }else{
      $this->moveFile($payload, $folder, $filename);
    }
    return $data;
  }

  private function checkFolder(){
    $year_folder = date('Y');
    $month_folder = date('m');
    $date_folder = date('d');
    $folder = 'uploads/' . (string)$year_folder . '/' . (string)$month_folder . "/" . (string)$date_folder;
    if (!file_exists($folder)) {
        mkdir($folder, 0777, true);
    }
    return $folder;
  }

  private function resizeImage($data, $img, $path){
    $temp_path = $path . '/temp';
    if (!file_exists($temp_path)) {
      mkdir($temp_path, 0777, true);
    }
    $temp_file = $temp_path . '/' . $data['edited_filename'];
    $thumb_file = $temp_path . '/thumbnail-' . $data['edited_filename'];
    \File::copy($data['path'], $temp_file);

    $image = Image::make($temp_file);
    if($data['original_width'] > 1024) {
      $width = 1024;
    }else{
      $width = $data['original_width'];
    }

    if ($data['original_height'] > 780) {
      $height = 780;
    }else{
      $height = $data['original_height'];
    }
    $image->fit($width, $height);
    $image->save($temp_file);

    $image = Image::make($temp_file);
    $image->fit(100, 100);
    $image->save($thumb_file);
  }

  private function moveFile($file, $path, $filename){
    $file->move($path, $filename);
  }

  private function generateFilename($file) {
    $filename = date('Ymdhis');
    $temp = (string)round((string)microtime(),5);
    $miliseconds = str_replace('0.','',$temp);
    return $filename.$miliseconds.'.'.$file->getClientOriginalExtension();
  }
}