<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\FileUploader;
use Illuminate\Support\Facades\Input;
use App\Attachment;
use App\Helpers\CheckPermission;

class ProductController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->uploader = new FileUploader();
    }
    public function index(){
        $attach = Attachment::get();
        return view('product.index', array('attachments'=>$attach));
    }

    public function getAdd(){
        // $this->checkPermission->check('create-post');
        return view('product.add');
    }

    public function create() {
        CheckPermission::check('create-post');
        return view('product.create');
    }

    public function store(Request $request) {
        $a = $request->file('attachment');
        foreach($a as $f){
            $ret = $this->uploader->upload($f);
            $attach = new Attachment();
            $attach->file_name = $ret['edited_filename'];
            $attach->file_type = $ret['filetype'];
            $attach->path = $ret['path'];
            $attach->save();
        }
        return view('product.index');
    }

    public function edit(Request $request) {
        CheckPermission::check('update-post');
        return view('product.edit');
    }
}
