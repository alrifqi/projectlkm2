<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(){
        $createPost = new \App\Permission();
        $createPost->name         = 'delete-post';
        $createPost->display_name = 'Delete Posts'; // optional
        $createPost->description  = 'delete new blog posts'; // optional
        $createPost->save();
    }

}
