<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index(){
        $role = new \App\Role();
        $role->name         = 'admin';
        $role->display_name = 'User Administrator'; // optional
        $role->description  = 'User is allowed to manage and edit other users'; // optional
        $role->save();
    }

    public function attach(){
        $role = \App\Role::find(1);
        $permission = \App\Permission::find(1);
        $role->attachPermission($permission);
    }
}
