<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\CheckPermission;

class CheckPermissionProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Helpers\CheckPermission', function ($app) {
            return new CheckPermission();
          });
    }
}
